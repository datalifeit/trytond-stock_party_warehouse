# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.parent.states.pop('invisible')

    @classmethod
    def _parent_domain(cls):
        # TODO: Remove parent field in 6.2 version
        parent_domain = super()._parent_domain()
        parent_domain['warehouse'].append('view')
        return parent_domain
