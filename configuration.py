# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelSQL, ValueMixin


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    party_warehouse_parent = fields.MultiValue(
        fields.Many2One('stock.location', 'Party warehouses Parent',
            domain=[('type', '=', 'view')]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'party_warehouse_parent':
            return pool.get('stock.configuration.party_warehouse')
        return super().multivalue_model(field)


class ConfigurationPartyWarehouse(ModelSQL, ValueMixin):
    """Stock Configuration Party Warehouse"""
    __name__ = 'stock.configuration.party_warehouse'

    party_warehouse_parent = fields.Many2One('stock.location',
        'Party warehouse Parent',
        domain=[('type', '=', 'view')])
