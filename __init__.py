# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import party
from . import stock
from . import shipment
from .model import ReturnableMoveMixin


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationPartyWarehouse,
        party.Party,
        party.CreateWarehouseStart,
        party.PartyWarehouse,
        stock.Location,
        shipment.ShipmentInReturn,
        shipment.ShipmentOut,
        shipment.ShipmentOutReturn,
        shipment.ShipmentInternal,
        module='stock_party_warehouse', type_='model')
    Pool.register(
        party.CreateWarehouse,
        module='stock_party_warehouse', type_='wizard')
